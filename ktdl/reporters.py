import json
import logging
from typing import MutableMapping, Any, List

from ktdl.utils import mappingbase

log = logging.getLogger(__name__)


class ReportMessage(mappingbase.MappingBase):
    def __init__(self, message: str, reporter: str = None, **params):
        super().__init__({'message': message, **params})
        self.params['reporter_name'] = reporter

    @property
    def message(self) -> str:
        return self['message']

    @property
    def params(self) -> MutableMapping[str, Any]:
        return self._params


class Reporters(mappingbase.MappingBase):
    def __init__(self, ktdl):
        super().__init__()
        self._ktdl = ktdl
        self._printers = dict(buffered=ReportBufferedPrinter, stream=ReportStreamedPrinter)

    @property
    def reporters(self) -> MutableMapping[str, 'Reporter']:
        return self._params

    @property
    def printers(self) -> MutableMapping[str, 'ReportPrinter']:
        return self._printers

    def instance(self, name, **kwargs) -> 'Reporter':
        if name not in self.params:
            self.params[name] = Reporter(name, ktdl=self._ktdl, **kwargs)
        return self.params[name]

    def __getitem__(self, key: str) -> 'Reporter':
        return self.instance(key)


class Reporter:
    def __init__(self, name: str, ktdl=None, **kwargs):
        self._name = name
        self._params = kwargs
        self._report_printers = []
        self._ktdl = ktdl

    def _resolve_printers(self) -> List['ReportPrinter']:
        printers = []
        if 'printers' not in self.params:
            return []
        for printer in self.params.get('printers'):
            if isinstance(printer, str):

    @property
    def report_printers(self) -> List['ReportPrinter']:
        return self._report_printers

    @property
    def params(self) -> MutableMapping[str, Any]:
        return self._params

    @property
    def name(self) -> str:
        return self._name

    def report(self, message: str, **params) -> 'ReportMessage':
        return self._report_message(ReportMessage(message, reporter=self.name, **params))

    def _report_message(self, msg: 'ReportMessage'):
        log.info(f"[REP] Reporting: {msg}")
        for printer in self.report_printers:
            printer.print(msg, reporter=self)
        return msg


class ReportPrinter:
    def print(self, message: 'ReportMessage', reporter=None):
        pass


class ReportBufferedPrinter(ReportPrinter):
    def __init__(self):
        self._messages = []

    @property
    def messages(self) -> List['ReportMessage']:
        return self._messages

    def print(self, message: 'ReportMessage', reporter=None):
        self.messages.append(message)

    def to_json(self) -> str:
        return json.dumps(self.messages)


class ReportStreamPrinter(ReportPrinter):
   pass