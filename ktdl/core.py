import collections
import enum
import json
import uuid
import io
from pathlib import Path
from typing import List, Optional, Any, MutableMapping, Union

import yaml

from ktdl import utils
from ktdl.utils import naming


class Result(enum.Enum):
    PASS = "pass"
    FAIL = "fail"
    SKIP = "skip"
    NONE = "none"
    ERROR = "error"


class TestAssert:
    def __init__(self, result: 'Result', desc: str):
        self._result = result.name
        self._desc = desc

    @property
    def result(self) -> 'Result':
        return Result(self._result)

    @property
    def desc(self) -> str:
        return self._desc


class TestAsserts:
    def __init__(self, test: 'KontrTest'):
        self._asserts = []
        self._test = test

    @property
    def test(self) -> 'KontrTest':
        return self._test

    def insert(self, index: int, object: 'TestAssert') -> None:
        self.asserts.insert(index, object)

    @property
    def asserts(self) -> List['TestAssert']:
        return self._asserts

    def check(self, result: 'Result', desc: str):
        self.asserts.append(TestAssert(result=result, desc=desc))


class Artifacts:
    def __init__(self, test: 'KontrTest'):
        self._test = test

    @property
    def test(self) -> 'KontrTest':
        return self._test

    @property
    def base_path(self) -> Path:
        return self.test.ktdl.config.base_path

    @property
    def path(self) -> Path:
        return self.base_path / self.test.namespace

    def save(self, name: str, stream: Union[str, io.BufferedReader]):
        full_path = self.path / name
        if isinstance(stream, str):
            full_path.write_text(encoding='utf-8')
        if isinstance(stream, io.BufferedReader):
            with full_path.open('w') as fd:
                fd.write(stream.read())

    def save_yaml(self, name: str, obj: MutableMapping):
        full_path = self.path / name
        with full_path.open('w') as fd:
            yaml.dump(obj, stream=fd)

    def save_json(self, name: str, obj: MutableMapping):
        full_path = self.path / name
        with full_path.open('w') as fd:
            json.dump(obj, fd)

    def text(self, name: str) -> str:
        full_path = self.path / name
        return full_path.read_text(encoding='utf-8')

    def bytes(self, name: str) -> bytes:
        full_path = self.path / name
        return full_path.read_bytes()

    def load_json(self, name: str) -> MutableMapping:
        return self.load_any(name, json.load)

    def load_yaml(self, name: str) -> MutableMapping:
        return self.load_any(name, yaml.load)

    def load_any(self, name: str, loader=None, **kwargs) -> Any:
        full_path = self.path / name
        with full_path.open('w') as fd:
            return loader(fd, **kwargs)


class KontrSuite(utils.MappingBase):
    def __init__(self, name: str, id: str = None):
        super().__init__({})
        self['name'] = name
        self['id'] = id if id is not None else uuid.uuid4()
        self._tests = []
        self._ktdl = None

    @property
    def ktdl(self):
        return self._ktdl

    @ktdl.setter
    def ktdl(self, val):
        self._ktdl = val

    @property
    def id(self) -> str:
        return self['id']

    @property
    def name(self) -> str:
        return self['name']

    @name.setter
    def name(self, val: str):
        self['name'] = val

    @property
    def tests(self) -> List['KontrTest']:
        return self._tests

    def add_test(self, test: 'KontrTest') -> 'KontrTest':
        test.suite = self
        test._ktdl = self.ktdl
        return test

    def create_test(self, **kwargs) -> 'KontrTest':
        return self.add_test(KontrTest(**kwargs))


class KontrTest(utils.MappingBase):
    def __init__(self, parent=None, id=None, name: str = None, desc: str = None, points: float = 0,
                 tags: List[str] = None):
        super().__init__({})
        self['id'] = id if id is not None else uuid.uuid4()
        self['name'] = name
        self['desc'] = desc
        self['points'] = points if points is not None else 0.0
        self['tags'] = tags if tags is not None else []
        self._subtests = []
        self._parent = parent
        self._result = Result.NONE
        self._asserts = TestAsserts(self)
        self._suite = None
        self._artifacts = Artifacts(self)

    @property
    def artifacts(self) -> 'Artifacts':
        return self._artifacts

    @property
    def suite(self) -> 'KontrSuite':
        return self._suite if self._suite is not None else (self.parent.suite if self.parent is not None else None)

    @suite.setter
    def suite(self, val: 'KontrSuite'):
        self._suite = val

    @property
    def ktdl(self):
        return self.suite.ktdl

    @property
    def asserts(self) -> TestAsserts:
        return self._asserts

    @property
    def result(self) -> 'Result':
        return Result(self._result)

    @result.setter
    def result(self, val: 'Result'):
        self.result = val.value

    @property
    def id(self) -> str:
        return self['id']

    @property
    def name(self) -> str:
        return self.get('name', f"test_{self.id}")

    @name.setter
    def name(self, val: str):
        self['name'] = val

    @property
    def codename(self) -> str:
        return naming.slugify(self.name)

    @property
    def desc(self) -> str:
        return self.get('desc', self.name.capitalize())

    @desc.setter
    def desc(self, val: str):
        self['desc'] = val

    @property
    def points(self) -> float:
        return self.get('points', 0.0)

    @points.setter
    def points(self, val: float):
        self['points'] = val

    @property
    def tags(self) -> List['str']:
        return self['tags']

    @property
    def subtests(self) -> List['KontrTest']:
        return self._subtests

    @property
    def parent(self) -> Optional['KontrTest']:
        return self._parent

    @parent.setter
    def parent(self, val: 'KontrTest'):
        self._parent = val

    @property
    def namespace(self) -> str:
        return (self.parent.namespace if self.parent else '') + '/' + self.codename

    @property
    def id_namespace(self) -> str:
        return (self.parent.id_namespace if self.parent else '') + '/' + self.id

    def add_test(self, test: 'KontrTest') -> 'KontrTest':
        self.subtests.append(test)
        test.parent = self
        test._ktdl = self.ktdl
        test.suite = self.suite
        return test

    def create(self, **kwargs) -> 'KontrTest':
        return self.add_test(KontrTest(**kwargs))


def flatten_all(tests: List['KontrTest']) -> List['KontrTest']:
    all_tests = []
    if not tests:
        return []

    for test in tests:
        all_tests.extend(flatten_all(test.subtests))
    return all_tests
