import collections.abc
import json
from typing import MutableMapping, Any, Iterator

import yaml


class MappingBase(collections.abc.MutableMapping):
    def __init__(self, params=None):
        self._params = {**(params or {})}

    @property
    def params(self) -> MutableMapping[str, Any]:
        return self._params

    def __setitem__(self, k: str, v: Any):
        self.params[k] = v

    def __delitem__(self, key: str) -> None:
        del self.params[key]

    def __getitem__(self, key: str) -> Any:
        return self.params.get(key)

    def __len__(self) -> int:
        return len(self._params)

    def __iter__(self) -> Iterator:
        return iter(self.params)

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}: {self.params}"

    def as_dict(self) -> MutableMapping[str, Any]:
        return self.params

    def as_json(self) -> str:
        return json.dumps(self.params)

    def as_yaml(self) -> str:
        return yaml.dump(self.params)

    def __str__(self) -> str:
        return str(self.as_dict())
