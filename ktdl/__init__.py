from pathlib import Path
from typing import List

from ktdl.utils import MappingBase

__version__ = '0.1.0'

from ktdl import core, log_config, reporters


class KtdlConfig(MappingBase):
    def __init__(self, results: str = None, reports: str = None, **kwargs):
        super().__init__(kwargs)
        self['results'] = Path(results)
        self['reports'] = Path(reports)

    @property
    def results(self) -> Path:
        return Path(self['results'])

    @property
    def reports(self) -> Path:
        return Path(self['reports'])


class KTDL:
    def __init__(self, config: 'KtdlConfig'):
        self._config = config
        self._suites = []
        log_config.initialize(**self.config.params)
        self._reporters = reporters.Reporters(self)

    @property
    def reporters(self) -> 'reporters.Reporters':
        return self._reporters

    @property
    def config(self) -> 'KtdlConfig':
        return self._config

    @property
    def suites(self) -> List[core.KontrSuite]:
        return self._suites

    def create_suite(self, name: str, id: str = None) -> core.KontrSuite:
        suite = core.KontrSuite(name=name, id=id)
        return self.add_suite(suite)

    def add_suite(self, suite: 'core.KontrSuite') -> core.KontrSuite:
        suite.ktdl = self
        self.suites.append(suite)
        return suite
