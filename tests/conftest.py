from pathlib import Path

import pytest

import ktdl


@pytest.fixture()
def base_dir(tmp_path):
    base_dir = tmp_path / 'ktdl_testing'
    base_dir.mkdir()
    return Path(base_dir)


@pytest.fixture()
def ktdl_config(base_dir) -> 'ktdl.KtdlConfig':
    results = base_dir / 'results'
    reports = base_dir / 'reports'
    return ktdl.KtdlConfig(results=results, reports=reports)


@pytest.fixture()
def ktdl_instance() -> 'ktdl.KTDL':
    return ktdl.KTDL(config=ktdl_config)
