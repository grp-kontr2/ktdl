import ktdl


def test_create_reporter(ktdl_instance: ktdl.KTDL):
    student = ktdl_instance.reporters['student']

    assert student is not None
    assert student.name == 'student'
    assert student.report_printers == []
